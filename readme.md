### Execution steps

* To run the executable it requires Qt runtime dlls and windows side-by-side dlls as mentioned in the manifest file. ```sqlite3.exe``` is 3rd party application used to manage local file system database.
* You can compile the code in _Qtcreator_ or _Visual Studio with Qt-VS addin_
* Qt link: http://www.qt.io/download/