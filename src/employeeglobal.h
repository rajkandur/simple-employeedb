#ifndef EMPLOYEEGLOBAL_H
#define EMPLOYEEGLOBAL_H

/*!brief
* Enum to arrange the details of the employee.
*/
enum DBDETAILS{
	FNAME = 0,
	LNAME,
	EID,
	EMAIL,
	ADDRESS
};

#endif //EMPLOYEEGLOBAL_H