#ifndef CEmployeeDB_H
#define CEmployeeDB_H

#include <QtGui/QDialog>
#include "ui_EmployeeDB.h"
#include "employeeglobal.h"

class CEmployeeDB : public QDialog
{
	Q_OBJECT

public:
	CEmployeeDB(QWidget *parent = 0, Qt::WFlags flags = 0);
	~CEmployeeDB();

private:
	Ui::EmployeeDBClass ui;

	static int nCount;
	const QString m_sSQLITEDB; 
	bool createDB(QString);
	bool updateinDB(QMap<DBDETAILS, QString>);
	bool createSQLSyntax(QMap<DBDETAILS, QString>, bool);
	void setStatusMessage(QString);

private slots:
	void onUpdateDB();
	void onRetrieveDetails();

};

#endif // CEmployeeDB_H
