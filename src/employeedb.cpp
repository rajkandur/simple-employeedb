#include "employeedb.h"
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QFile>
#include <QProcess>

/*!brief
nCount = 0; // first time creating the DB
nCount !=0; // updating the DB
*/
int CEmployeeDB::nCount = 0;

CEmployeeDB::CEmployeeDB(QWidget *parent, Qt::WFlags flags)
: QDialog(parent, flags), m_sSQLITEDB("EMPLOYEEDB.db")
{
	ui.setupUi(this);

	//connect the statements of 'UpdateDatabase' and 'Retrieve'
	connect(this->ui.updateDBpushButton, SIGNAL(clicked()), this, SLOT(onUpdateDB()));
	connect(this->ui.retrievepushButton, SIGNAL(clicked()), this, SLOT(onRetrieveDetails()));

	ui.empIDlineEdit->setFocus();
	setTabOrder(ui.empIDlineEdit, ui.fnlineEdit);
	setTabOrder(ui.fnlineEdit, ui.lnlineEdit);
	setTabOrder(ui.lnlineEdit, ui.emaillineEdit);
	setTabOrder(ui.emaillineEdit, ui.addresstextEdit);
	setTabOrder(ui.addresstextEdit, ui.updateDBpushButton);
	setTabOrder(ui.updateDBpushButton, ui.emprlineEdit);
	setTabOrder(ui.emprlineEdit, ui.textEdit);
	setTabOrder(ui.textEdit, ui.retrievepushButton);
}

CEmployeeDB::~CEmployeeDB()
{

}

void CEmployeeDB::onUpdateDB()
{
	//get First, Last names, empid, email and address
	QMap<DBDETAILS, QString> empMap;
	empMap.clear();
	empMap.insert(FNAME,ui.fnlineEdit->text().trimmed());
	empMap.insert(LNAME,ui.lnlineEdit->text().trimmed());
	empMap.insert(EID,ui.empIDlineEdit->text().trimmed());
	empMap.insert(EMAIL,ui.emaillineEdit->text().trimmed());
	empMap.insert(ADDRESS,ui.addresstextEdit->toPlainText().trimmed());

	//validate it
	if( empMap.isEmpty() ||
		ui.fnlineEdit->text().trimmed().isEmpty() ||
		ui.lnlineEdit->text().trimmed().isEmpty() ||
		ui.empIDlineEdit->text().trimmed().isEmpty() ||
		ui.emaillineEdit->text().trimmed().isEmpty() )
	{
		setStatusMessage("Fill the empty fields.");
		return;
	}

	//create SQL syntax file.
	bool bReturnStatus;
	if( 0 == nCount )
		bReturnStatus = createSQLSyntax(empMap, true);
	else
		bReturnStatus = createSQLSyntax(empMap, false);
	
	//send the details to update in DB
	if(!bReturnStatus)
	{
		setStatusMessage("Unable to update/create the database.");
		return;
	}

	nCount = 1;
	return;
}

bool CEmployeeDB::createSQLSyntax(QMap<DBDETAILS, QString> empMap, bool bFirstTime)
{
	QString sSQLTableHeader = QString("BEGIN TRANSACTION;" 
    "CREATE TABLE Employee (FIRSTNAME TEXT, LASTNAME TEXT, EID INTEGER PRIMARY KEY, EMAIL TEXT, ADDRESS TEXT);"
    "INSERT INTO Employee VALUES('%1','%2',%3,'%4','%5');"
	"COMMIT;").arg(empMap[FNAME]).arg(empMap[LNAME]).arg(empMap[EID]).arg(empMap[EMAIL]).arg(empMap[ADDRESS]);

	bool bStatus;
	if(bFirstTime)
		bStatus = createDB(sSQLTableHeader);
	else
		bStatus = updateinDB(empMap);

	return bStatus;
}

bool CEmployeeDB::updateinDB(QMap<DBDETAILS, QString> empMap)
{
	QSqlDatabase sqlempDB = QSqlDatabase::addDatabase("QSQLITE");
	sqlempDB.setDatabaseName(m_sSQLITEDB);

	if(!sqlempDB.open())
		return false;

	//execute a query
	QSqlQuery query(sqlempDB);
	query.prepare("INSERT INTO Employee (FIRSTNAME, LASTNAME, EID, EMAIL, ADDRESS) "
		"VALUES (?, ?, ?, ?, ?)");
	query.addBindValue(empMap[FNAME]);
	query.addBindValue(empMap[LNAME]);
	query.addBindValue(empMap[EID]);
	query.addBindValue(empMap[EMAIL]);
	query.addBindValue(empMap[ADDRESS]);
	
	if( !query.exec() )
	{
		setStatusMessage( query.lastError().text() );
		return false;
	}

	setStatusMessage( "Record inserted successfully.");
	return true;
}

bool CEmployeeDB::createDB(QString sData)
{
	QProcess* processDB = new QProcess();
	QString sSQLDataFilename = "sqlcommands.txt";

	//write the sqlcommands file
	QFile file(sSQLDataFilename);

	if(!file.open(QIODevice::WriteOnly|QIODevice::Text))
	{
		setStatusMessage("Unable to create database file.");
		return false;
	}

	file.write(sData.toAscii().data());
	file.close();

	//create a sqlite db file
	file.setFileName("temp.bat");
	QString sCmd = QString("sqlite3.exe %1 < %2").arg(m_sSQLITEDB).arg(sSQLDataFilename);

	if(!file.open(QIODevice::WriteOnly|QIODevice::Text))
	{
		setStatusMessage("Unable to create database file.");
		return false;
	}

	file.write(sCmd.toAscii().data());
	file.close();

	processDB->start("temp.bat");
	processDB->waitForStarted(5000);
	processDB->waitForFinished(5000);
	
	if( QProcess::Running == processDB->state() )
		processDB->kill();

	//delete the temporary files created
	QFile(sSQLDataFilename).remove();
	file.remove();

	setStatusMessage( "Record inserted successfully.");
	return true;
}

void CEmployeeDB::onRetrieveDetails()
{
	ui.textEdit->clear();

	QSqlDatabase sqlempDB = QSqlDatabase::addDatabase("QSQLITE");
	sqlempDB.setDatabaseName(m_sSQLITEDB);

	if(!sqlempDB.open())
	{
		setStatusMessage("Unable to retrieve the data for the employee id.");
		return;
	}

	QString sEID = ui.emprlineEdit->text().trimmed();

	//execute a query
	QSqlQuery query(sqlempDB);
	query.prepare(QString("SELECT * FROM Employee WHERE EID='%1'").arg(sEID));
	
	if( !query.exec() )
	{
		setStatusMessage( query.lastError().text() );
		return;
	}

	QStringList recList;
	bool bStatus = false;
	while(query.next())
	{
		bStatus = true;

		recList.append(query.value(0).toString());
		recList.append(query.value(1).toString());
		recList.append(query.value(2).toString());
		recList.append(query.value(3).toString());
		recList.append(query.value(4).toString());
	}

	if(!bStatus)
	{
		setStatusMessage("Data does not exist for the entered empID.");
		ui.textEdit->setText("No data found.");
		return;
	}

	QString sData = "<b>EmpID:</b> " + recList.at(2).trimmed() + "\r\n";
	sData.append("<b>FirstName:</b> ");
	sData.append(recList.at(0).trimmed());
	sData.append("\r\n");

	sData.append("<b>LastName:</b> ");
	sData.append(recList.at(1).trimmed());
	sData.append("\r\n");

	sData.append("<b>Email:</b> ");
	sData.append(recList.at(3).trimmed());
	sData.append("\r\n");

	sData.append("<b>Address:</b> ");
	sData.append(recList.at(4).trimmed());
	sData.append("\r\n");

	ui.textEdit->setText(sData);
	setStatusMessage("Record retrieved successfully.");
}

void CEmployeeDB::setStatusMessage(QString sStatusMsg)
{
	sStatusMsg.prepend("<b>");
	sStatusMsg.append("<b>");
	ui.statusMsglabel->setText(sStatusMsg);
	ui.statusMsglabel->setTextFormat(Qt::RichText);
}
